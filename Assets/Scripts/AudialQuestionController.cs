﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class AudialQuestionController : MonoBehaviour {

    private GameObject InfoText;
    private GameObject ListenAuido;
    private AudioSource AudioSource;
    public AudioClip[] clips;

 

    // Use this for initialization
    void Start () {
        InfoText = GameObject.Find("InfoText");
        ListenAuido = GameObject.Find("ListenAudio");

        SetInfoText("Ses kaydını dinlemek için tek hakkınız vardır. \n Kayıt bittikten sonra oyun başlayacaktır.");

        AudioSource = GetComponent<AudioSource>();
        AudioSource.clip = clips[0];
        AudioSource.Play();
        DisableListenAudio();
        Invoke("EnableListenAudio", AudioSource.clip.length);

        ListenAuido.GetComponent<Button>().onClick.AddListener(ListenAudioPressed);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void ListenAudioPressed()
    {
        AudioSource.clip = clips[1];
        AudioSource.Play();
        DisableListenAudio();
        Invoke("LoadGameScene", AudioSource.clip.length);

    }

    void SetInfoText(string info)
    {
        InfoText.GetComponent<Text>().text = info;
    }

    void LoadGameScene()
    {
        SceneManager.LoadScene("TextualGame");
    }

    void DisableListenAudio()
    {
        CanvasGroup ListenAuidoCanvasGroup = ListenAuido.GetComponent<CanvasGroup>();
        ListenAuidoCanvasGroup.interactable = false;
    }

    void EnableListenAudio()
    {
        CanvasGroup ListenAuidoCanvasGroup = ListenAuido.GetComponent<CanvasGroup>();
        ListenAuidoCanvasGroup.interactable = true;
    }
}
