﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VisualQuestionController : MonoBehaviour {

    private GameObject InfoText;
    private GameObject SeePictures;
    private GameObject Image;
    public Sprite [] Images;
    private bool showImage = false;
    private float timer = 2.0f;
    int index = 0;

    // Use this for initialization
    void Start () {
        InfoText = GameObject.Find("InfoText");
        SeePictures = GameObject.Find("SeePictures");
        Image = GameObject.Find("Image");

        ShowInfoText();
        HideImage();

        SetInfoText("Resimleri görmek için tek hakkınız vardır. \n Resimler bittikten sonra oyun başlayacaktır.");
        SeePictures.GetComponent<Button>().onClick.AddListener(SeePicturesPressed);
    }
	
	// Update is called once per frame
	void Update () {        
        if (showImage)
        {
            timer += Time.deltaTime;
            if (timer >= 2.0f && index < 5)
            {
                timer = 0f;
                Image.GetComponent<Image>().sprite = Images[index];
                index += 1;
            } else if(timer >= 2.0f && index == 5){
                HideImage();
                LoadGameScene();
            }
        }
		
	}

    void SeePicturesPressed()
    {
        DisableSeePictures();
        ShowImage();
        HideInfoText();
        showImage = true;

        //int index = Random.Range(1, 5);

        //Image.GetComponent<Image>().sprite = Images[index];
        //Invoke("LoadGameScene", 7);
    }

    void SetInfoText(string info)
    {
        InfoText.GetComponent<Text>().text = info;
    }

    void LoadGameScene()
    {
        SceneManager.LoadScene("TextualGame");
    }
    
    void HideImage()
    {
        CanvasGroup ImageCanvasGroup = Image.GetComponent<CanvasGroup>();
        ImageCanvasGroup.alpha = 0;
        ImageCanvasGroup .interactable = false;
    }

    void ShowImage()
    {
        CanvasGroup ImageCanvasGroup = Image.GetComponent<CanvasGroup>();
        ImageCanvasGroup.alpha = 1;
        ImageCanvasGroup.interactable = true;
    }
    void HideInfoText()
    {
        CanvasGroup InfoTextCanvasGroup = InfoText.GetComponent<CanvasGroup>();
        InfoTextCanvasGroup.alpha = 0;
        InfoTextCanvasGroup.interactable = false;
    }

    void ShowInfoText()
    {
        CanvasGroup InfoTextCanvasGroup = InfoText.GetComponent<CanvasGroup>();
        InfoTextCanvasGroup.alpha = 1;
        InfoTextCanvasGroup.interactable = true;
    }

    void DisableSeePictures()
    {
        CanvasGroup SeePicturesCanvasGroup = SeePictures.GetComponent<CanvasGroup>();
        SeePicturesCanvasGroup.interactable = false;
    }

    void EnableSeePictures()
    {
        CanvasGroup SeePicturesCanvasGroup = SeePictures.GetComponent<CanvasGroup>();
        SeePicturesCanvasGroup.interactable = true;
    }
}

