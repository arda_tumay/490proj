﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkedList  {

    Node Head;
    Node Tail;
    int count;


    public class Node
    {
        object Data;
        Node Next;
        Node Previous;

        public Node()
        {
            Data = null;
            Next = null;
            Previous = null;
        }
        public Node(object data)
        {
            Data = data;
            Next = null;
            Previous = null;
        }
        public void SetNext(Node next)
        {
            this.Next = next;
        }
        public void SetPrev(Node Prev)
        {
            this.Previous = Prev;
        }
        public Node GetNext()
        {
            return Next;
        }
        public Node GetPrevious()
        {
            return Previous;
        }
        public object GetData()
        {
            return Data;
        }
        public override string ToString()
        {
            return Data.ToString();
        }
    }

    public LinkedList()
    {
        Head = null;
        Tail = null;
        count = 0;
    }

    public void AddNode(object data)
    {
        if(Head == null)// meaining that linked list is empty
        {
            Node newNode = new Node(data);
            Head = newNode;
            Tail = Head;
        }
        else
        {
            Node newNode = new Node(data);
            Tail.SetNext(newNode);
            newNode.SetPrev(Tail);
            Tail = newNode;
        }
        count++;
    }

    public Node GetTail()
    {
        return Tail;
    }
    public Node GetHead()
    {
        return Head;
    }
    public int GetSize()
    {
        return count;
    }

    public object[] toArray()
    {
        int i = 0;
        object [] arr = new object[count];
        Node temp = Head;
        while (temp != null)
        {
            arr[i] = temp.GetData();
            if (temp.GetNext() == null)
            {
                break;
            }
            temp = temp.GetNext();
            i++;
        }
        return arr;
    }

    public override string ToString()
    {
        Node temp = Head;
        string print = "";
        while(temp != null)
        {
            print = print + temp.ToString();
            if(temp.GetNext() == null)
            {
                break;
            }
            temp = temp.GetNext();
        }
        return print;
    }

}
