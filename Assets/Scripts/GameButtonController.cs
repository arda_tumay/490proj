﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameButtonController : MonoBehaviour {

    public string buttonText;
    public bool isAnswer;

    void Awake()
    {

    }

    // Use this for initialization
    void Start () {
        gameObject.GetComponent<Button>().onClick.AddListener(ButtonClicked);
        if (isAnswer)
        {
            SetAnswer();
        }
    }
	
	// Update is called once per frame
	void Update () {
        
    }

    void ButtonClicked()
    {

        Answer ans = new Answer(buttonText, isAnswer);
        object[] param = { ans, gameObject };
        GameObject.Find("Canvas").GetComponent<GameCanvasController>().SendMessage("AnswerClicked", param);
        if (isAnswer)
        {
            SetColor(Color.green);
            gameObject.GetComponent<Button>().interactable = false;
        }
        else
        {
            SetColor(Color.red);
            gameObject.GetComponent<Button>().interactable = false;
        }
    }

    void SetAnswer()//make the canvas controller know all answers at the beginning of the game
    {
        Answer ans = new Answer(buttonText, isAnswer);
        GameObject.Find("Canvas").GetComponent<GameCanvasController>().SendMessage("SetAnswer", ans);
    }

    void MakeAnswerFailed()
    {
        SetColor(Color.grey);
        gameObject.GetComponent<Button>().interactable = false;
    }

    void SetColor(Color buttonColor)
    {
        GetComponent<Image>().color = buttonColor;
    }

    public Button GetButtonByText(string ansText)
    {
        return ansText == buttonText ? gameObject.GetComponent<Button>() : null;
    }

}
