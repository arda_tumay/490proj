﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Answer  {

    public  float pressedTime { get; private set; } //the time that the user press the answer button
    public string text {  get; private set; } //text of the answer. Used as a primary key
    public bool isCorrect { get; private set; } // whether the answer is correct or not
    public bool isClickedByUser { get; private set; } // whether the user clicks the button 
    public int hintCount { get; private set; }
    public bool failed { get; private set; }//user couldnt know the answer even if he is given with 2 hints
    public string currentHint { get; private set; }

    public Answer(float pressTime, string text, bool isCorrect)
    {
        pressedTime = pressTime;
        this.text = text;
        this.isCorrect = isCorrect;
        hintCount = 0;
        isClickedByUser = false;
        failed = false;
        currentHint = "";
    }

    public Answer(string text, bool isCorrect)
    {
        this.pressedTime= 0.0f;
        this.text = text;
        this.isCorrect = isCorrect;
        hintCount = 0;
        isClickedByUser = false;
        failed = false;
        currentHint = "";
    }



    public override string ToString()
    {
        string toString = "Answer: " + text + "\n" +
            "Pressed time: " + pressedTime + "\n" +
            "Is Correct Answer: " + isCorrect + "\n" +
            "Is clicked by user: " + isClickedByUser + "\n" +
            "How many hints: " + hintCount + "\n" +
            "Does user fail to know answer: " + failed + "\n" +
            "Current hint: " + currentHint + "\n";
        return toString;
    }

    public void SetPressedTime(float time)
    {
        pressedTime = time;
    }

    public void SetHintCount(int hint)
    {
        hintCount = hint;
    }

    public void setIsClickedByUser(bool isFound)
    {
        isClickedByUser = isFound;
    }
    
    public void SetFailed(bool fail)
    {
        failed = fail;
    }

    public void SetCurrentHintTopic(string currentHint)
    {
        this.currentHint = currentHint;
    }

    public bool IsCorrect()
    {
        return isCorrect;
    }

    
}
