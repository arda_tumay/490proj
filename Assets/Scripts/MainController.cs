﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainController : MonoBehaviour {

    private GameObject Audial;
    private GameObject Visual;
    private GameObject InfoText;
    private GameObject StartTest;
    private string SelectedTestMode;
    private AudioSource AudioSource;



    // Use this for initialization
    void Start () {
        Audial = GameObject.Find("IsitselTest");
        Visual = GameObject.Find("GorselTest");
        InfoText = GameObject.Find("InfoText");
        StartTest = GameObject.Find("StartTest");

        AudioSource = GetComponent<AudioSource>();
        AudioSource.Stop();


        HideInfoText();
        HideStartTest();

        Audial.GetComponent<Button>().onClick.AddListener(AudialPressed);
        Visual.GetComponent<Button>().onClick.AddListener(VisualPressed);
        StartTest.GetComponent<Button>().onClick.AddListener(StartTestPressed);


    }

    // Update is called once per frame
    void Update () {
		
	}

    void AudialPressed()
    {
        SelectedTestMode = "audial";
        AudioSource.Play();
        HideMenuButtons();
        ShowInfoText();
        HideInfoText();
        //SetInfoText("Birazdan çeşitli kelimelerin söylendiği bir ses kaydı dinleyeceksiniz.\nArdından sizden bu kelimeleri hatırlamanız ve karşınıza çıkacak olan listeden hatırladığınız kelimeleri işaretlemeniz istenecek.\nArdından Hatırladığınız kelimelerin görsellerini işaretlemeniz istenecek");
        ShowStartTest();
        DisableStartTest();
        Invoke("EnableStartTest", AudioSource.clip.length);
    }

    void VisualPressed()
    {
        SelectedTestMode = "visual";
        HideMenuButtons();
        ShowInfoText();
        SetInfoText("Birazdan çeşitli resimler göreceksiniz.\nArdından sizden bu resimlerde ne olduğunu hatırlamanız ve karşınıza çıkacak olan listeden hatırladığınız cisimleri işaretlemeniz istenecek.\nArdından Hatırladığınız cisimlerin görsellerini işaretlemeniz istenecek");
        ShowStartTest();
    }

    void StartTestPressed()
    {
        if (SelectedTestMode == "visual") {
            GameCanvasController.gameMod = 1;
            LoadVisualQuestionScene();
        }
        else {
            GameCanvasController.gameMod = 0;
            LoadAudialQuestionScene();
        }

    }


    void LoadVisualQuestionScene()
    {
        SceneManager.LoadScene("VisualQuestion");
    }

    void LoadAudialQuestionScene()
    {
        SceneManager.LoadScene("AudialQuestion");
    }

    void HideMenuButtons()
    {
        CanvasGroup AudialCanvasGroup =  Audial.GetComponent<CanvasGroup>();
        AudialCanvasGroup.alpha = 0;
        AudialCanvasGroup.interactable = false;

        CanvasGroup VisualCanvasGroup = Visual.GetComponent<CanvasGroup>();
        VisualCanvasGroup.alpha = 0;
        VisualCanvasGroup.interactable = false;
    }

    void HideInfoText()
    {
        CanvasGroup InfoTextCanvasGroup = InfoText.GetComponent<CanvasGroup>();
        InfoTextCanvasGroup.alpha = 0;
        InfoTextCanvasGroup.interactable = false;
    }

    void ShowInfoText()
    {
        CanvasGroup InfoTextCanvasGroup = InfoText.GetComponent<CanvasGroup>();
        InfoTextCanvasGroup.alpha = 1;
        InfoTextCanvasGroup.interactable = true;
    }

    void SetInfoText(string info)
    {
        InfoText.GetComponent<Text>().text = info;
    }
    
    void HideStartTest()
    {
        CanvasGroup StartTestCanvasGroup = StartTest.GetComponent<CanvasGroup>();
        StartTestCanvasGroup.alpha = 0;
        StartTestCanvasGroup.interactable = false;
    }

    void ShowStartTest()
    {
        CanvasGroup StartTestCanvasGroup = StartTest.GetComponent<CanvasGroup>();
        StartTestCanvasGroup.alpha = 1;
        StartTestCanvasGroup.interactable = true;
    }
    
    void DisableStartTest()
    {
        CanvasGroup StartTestCanvasGroup = StartTest.GetComponent<CanvasGroup>();
        StartTestCanvasGroup.interactable = false;
    }

    void EnableStartTest()
    {
        CanvasGroup StartTestCanvasGroup = StartTest.GetComponent<CanvasGroup>();
        StartTestCanvasGroup.interactable = true;
    }
}
