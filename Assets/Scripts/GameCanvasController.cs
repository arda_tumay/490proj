﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameCanvasController : MonoBehaviour{

    
    Dictionary<string, Answer> answers;// true answers. initially set
    float time;
    LinkedList answersLinkedList;

    string currentHintAnswer;// which answer is given by hint. stores answer text
    int hintCounter;// how many times the user is given with the hint
    int secondHintCounter;// that counts to given hints pers topic. to run the audios. can onyl be 1 or 2.
    float hintTime;//hit counter
    bool allAnswersFound;
    bool setHintCounter;
    GameObject finishedText;
    GameObject hintText;


    GameObject currentPressedButton;
    AudioClip hintAudio;

    public static int gameMod;//0 = audial, 1 = visual

    Dictionary<string, string> textualHints;

    void Awake()
    {
        answers = new Dictionary<string, Answer>();
        answersLinkedList = new LinkedList(); ;
        time = 0.0f;
        hintTime = 0.0f;
        hintCounter = 0;
        secondHintCounter = 0;
        currentHintAnswer = "";
        allAnswersFound = false;
        finishedText = GameObject.Find("TestFinished");
        finishedText.GetComponent<CanvasGroup>().alpha = 0;
        hintText  = GameObject.Find("HintText");
        hintText.GetComponent<CanvasGroup>().alpha = 0;
        setHintCounter = true;
        if(gameMod == 1)
        {
            textualHints = new Dictionary<string, string>();
            textualHints.Add("cami1", "Bir yapı çeşidi.");
            textualHints.Add("cami2", "Apartman, cami, hastane");
            textualHints.Add("papatya1", "Bir çiçek çeşidi.");
            textualHints.Add("papatya2", "Menekşe, papatya, gül.");
            textualHints.Add("kadife1", "Bir kumaş türü.");
            textualHints.Add("kadife2", "Kadife, keten, kot.");
            textualHints.Add("mor1", "Bir renk çeşidi.");
            textualHints.Add("mor2", "Kırmızı, mor, pembe.");
            textualHints.Add("burun1", "Yüzümüzde bulunan bir organ.");
            textualHints.Add("burun2", "Ağız, burun, göz.");
        }
    }


    // Use this for initialization
    void Start () {
        //correctUserAnswers = new ArrayList();
        //wrongUserAnswers = new ArrayList();
        
    }
	
	// Update is called once per frame
	void Update () {
        if(hintTime > 5.0f && hintCounter < 2)
        {
            SetHintTopic();
            GiveHint();
        }else if(hintCounter == 2 && hintTime > 5.0f)// user is given with hints 2 times
        {
            MakeCurrentHintFail();
        }
        allAnswersFound = IsFinishedWithSuccess();
        if (allAnswersFound)
        {
            WriteFile();
            finishedText.GetComponent<CanvasGroup>().alpha = 1;
        }
        allAnswersFound  = IsFinishedWithFailure();
        if (allAnswersFound)
        {
            WriteFile();
            finishedText.GetComponent<CanvasGroup>().alpha = 1;
        }
    }

    void FixedUpdate()
    {
        time += Time.deltaTime;
        if (setHintCounter)
        {
            hintTime += Time.deltaTime;
        }
    }

    void SetHintTopic()
    {
        foreach (KeyValuePair<string, Answer> pair in answers)//traverse on true answers and pick one answer to give hint
        {
            if (!pair.Value.isClickedByUser && !pair.Value.failed)// if the user does not find the true answer, give hint about it
            {
                //CHECK WHETHER THERE IS ANY UNKNOWN ANSWERS
                currentHintAnswer = pair.Value.text;
                return ;
            }
        }
    }

    void GiveHint()
    {
        // GIVE THE HINT IN THIS FUNC.
        if (secondHintCounter == 2)
        {
            secondHintCounter = 0;
        }
        hintTime = 0.0f;
        setHintCounter = false;
        hintCounter++;
        secondHintCounter++;
        Debug.Log(currentHintAnswer);
        string audioName = currentHintAnswer + secondHintCounter;
        if (gameMod == 0)//audial hint
        {            
            Debug.Log(audioName);
            hintAudio = (AudioClip)Resources.Load("HintAudios/" + audioName);
            GetComponent<AudioSource>().clip = hintAudio;
            GetComponent<AudioSource>().Play();
            Invoke("ResetHintCounter", GetComponent<AudioSource>().clip.length);
        }else if (gameMod == 1) // visual hint
        {
            Debug.Log(audioName);
            hintText.GetComponent<Text>().text = textualHints[audioName];
            hintText.GetComponent<CanvasGroup>().alpha = 1;
            Invoke("ResetHintCounter", 3.0f);
            Invoke("HideHintText", 3.0f);

        }
    }

    void ResetHintCounter()
    {
        setHintCounter = true;
    }

    void HideHintText()
    {
        hintText.GetComponent<CanvasGroup>().alpha = 0;
    }

    bool IsFinishedWithSuccess()
    {
        bool allAnswered = true;
        foreach (KeyValuePair<string, Answer> pair in answers)//traverse on true answers and pick one answer to give hint
        {
            if (!pair.Value.isClickedByUser && !pair.Value.failed)// if the user does not find the true answer, give hint about it
            {
                allAnswered = false;
                return allAnswered;
            }
        }
        return allAnswered;
    }

    bool IsFinishedWithFailure()
    {
        bool allFailed = true;
        foreach (KeyValuePair<string, Answer> pair in answers)//traverse on true answers and pick one answer to give hint
        {
            if (!pair.Value.isClickedByUser && !pair.Value.failed)// if the user does not find the true answer, give hint about it
            {
                allFailed = false;
                return allFailed;
            }
        }
        return allFailed;
    }

    void SetAnswer(Answer ans)//method for initally knowing which answer true which not. Triggered by GameButtonController.
    {
        answers.Add(ans.text, ans);
    }

    void AnswerClicked(object [] param)
    {
        Answer answer = (Answer)param[0];
        currentPressedButton = (GameObject)param[1];
        if (currentHintAnswer != "")// meaining that there is an answer that is given hint for the user
        {
            if(answer.text == currentHintAnswer)// the user is find the true answer related to the hint.
            {
                hintTime = 0.0f;
                answer.SetPressedTime(time);
                answer.SetHintCount(hintCounter);
                answer.setIsClickedByUser(true);
                answer.SetCurrentHintTopic(currentHintAnswer);
                answersLinkedList.AddNode(answer);
                MakeAnswerFound(answer); // No need to check wheter answer is correct. because each hint is for correct answers
                currentHintAnswer = "";
                hintCounter = 0;
                secondHintCounter = 0;
            }
            else// the user is given with some hint but user does not check the true answer related to hint. clicks some other answer
            {
                answer.SetPressedTime(time);
                answer.SetHintCount(0);// other answers are not given by hint so hint count will be 0
                answer.setIsClickedByUser(true);
                answersLinkedList.AddNode(answer);
                answer.SetCurrentHintTopic(currentHintAnswer);
                if (answer.isCorrect) {
                    MakeAnswerFound(answer);
                    secondHintCounter = 0;
                }

                    
            }
        }
        else
        {
            hintTime = 0.0f;
            answer.SetPressedTime(time);
            answer.SetHintCount(hintCounter);
            answer.setIsClickedByUser(true);
            answersLinkedList.AddNode(answer);
            answer.SetCurrentHintTopic(currentHintAnswer);
            if (answer.isCorrect) MakeAnswerFound(answer);
        }
        //Debug.Log(answersLinkedList);
        
    }
    
    void MakeAnswerFound(Answer ans)
    {
        answers[ans.text].setIsClickedByUser(true);//set the answer as known by user by setting its IsFoundByUser property. required for hints
    }

    void MakeCurrentHintFail()
    {
        Debug.Log(currentHintAnswer + " FAILED");
        hintCounter = 0;
        hintTime = 0.0f;
        answers[currentHintAnswer].SetFailed(true);
        answers[currentHintAnswer].SetHintCount(2);
        answers[currentHintAnswer].setIsClickedByUser(false);
        answers[currentHintAnswer].SetPressedTime(time);
        answers[currentHintAnswer].SetCurrentHintTopic(currentHintAnswer);
        answersLinkedList.AddNode(answers[currentHintAnswer]);
        GameObject.Find(currentHintAnswer).GetComponent<GameButtonController>().SendMessage("MakeAnswerFailed");

        /*Button failedButton = GameObject.FindWithTag("AnswerButton").GetComponent<GameButtonController>().GetButtonByText(answers[currentHintAnswer].text);
        if(failedButton != null)
        {
            failedButton.GetComponentInParent<GameButtonController>().SendMessage("MakeAnswerFailed");
        }*/
        currentHintAnswer = "";
    }

    void WriteFile()
    {
        string print = "";
        object [] answerArray = answersLinkedList.toArray();
        foreach(object ans in answerArray)
        {
            print = print + ans.ToString() + "\n";
        }
        // WriteAllText creates a file, writes the specified string to the file,
        // and then closes the file.    You do NOT need to call Flush() or Close().
        if (SceneManager.GetActiveScene().name == "TextualGame")
        {
            System.IO.File.WriteAllText(@"TextualGameAnswers.txt", print);
            Invoke("LoadVisualGame", 2.0f);
        }else if(SceneManager.GetActiveScene().name == "VisualGame")
        {
            System.IO.File.WriteAllText(@"VisualGameAnswers.txt", print);
            Invoke("LoadEndGame", 2.0f);
        }
    }

    void LoadVisualGame()
    {
        SceneManager.LoadScene("VisualGame");
    }

    void LoadEndGame()
    {
        SceneManager.LoadScene("EndGame");
    }

    
}
